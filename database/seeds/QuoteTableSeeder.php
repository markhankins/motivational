<?php

use App\Models\Quote;
use Illuminate\Database\Seeder;

class QuoteTableSeeder extends Seeder
{
    public function run()
    {
        Quote::create([
            'text' => 'Success is going from failure to failure without losing your enthusiasm',
            'author' => 'Winston Churchill',
            'background' => '1.jpg'
        ]);

        Quote::create([
            'text' => 'Dream big and dare to fail',
            'author' => 'Norman Vaughan',
            'background' => '2.jpg'
        ]);

        Quote::create([
            'text' => 'It does not matter how slowly you go as long as you do not stop',
            'author' => 'Confucius',
            'background' => '3.jpg'
        ]);

        Quote::create([
            'text' => 'Failure will never overtake me if my determination to succeed is strong enough',
            'author' => 'Og Mandino',
            'background' => '4.jpg'
        ]);

        Quote::create([
            'text' => 'What you do today can improve all your tomorrows.',
            'author' => 'Ralph Marston',
            'background' => '5.jpg'
        ]);

        Quote::create([
            'text' => 'In order to succeed, we must first believe that we can',
            'author' => 'Nikos Kazantzakis',
            'background' => '6.jpg'
        ]);

        Quote::create([
            'text' => 'Aim for the moon. If you miss, you may hit a star',
            'author' => 'W. Clement Stone',
            'background' => '7.jpg'
        ]);

        Quote::create([
            'text' => "Don't watch the clock; do what it does. Keep going.",
            'author' => 'Sam Levenson',
            'background' => '8.jpg'
        ]);
    }
}